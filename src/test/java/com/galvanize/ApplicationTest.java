package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {

    private LightSaber lightSaber;

    @BeforeEach
    public void setUp() {
        lightSaber = new LightSaber(1234567);
    }

    @Test
    void testSetCharge() {
        lightSaber.setCharge(100);
        assertEquals(100, lightSaber.getCharge());
    }

    @Test
    void testGetCharge() {
        lightSaber.setCharge(100);

        float actual = lightSaber.getCharge();
        float expected = 100;

        assertEquals(expected, actual);
    }

    @Test
    void testSetColor() {
        lightSaber.setColor("Blue");
        assertEquals("Blue", lightSaber.getColor());
    }

    @Test
    void testGetColor() {
        lightSaber.setColor("Blue");

        String actual = lightSaber.getColor();
        String expected = "Blue";

        assertEquals(expected, actual);
    }

    @Test
    void testJediSerialNumber() {
        long actual = lightSaber.getJediSerialNumber();
        long expected = 1234567;

        assertEquals(expected, actual);
    }

    @Test
    void testUse() {
        lightSaber.use(100);

        assertEquals(249.99998f, lightSaber.getRemainingMinutes());
    }

    @Test
    void testGetMinutesRemaining() {
        assertEquals(300, lightSaber.getRemainingMinutes());
    }

    @Test
    void testRecharge() {
        lightSaber.use(100);
        lightSaber.recharge();
        assertEquals(100.0f, lightSaber.getCharge());
    }
}